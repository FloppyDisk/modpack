# Republicraft Modpack

Dans chaque modpack, vous trouverez : 

- L'ip du Serveur
- Un Ressource pack (Faithful 32) : http://f32.me/

## Versions du Modpack

- 1.6.4
- 1.12.2

Chacune des versions sont disponible sur le repository.
Pour selectionner celle que vous souhaitez, changez la branche master en la version souhaitée.

## Authors

- Shiroe_sama (Alexandre Caillot)
